package HW5_3;

public class Main {

    @Min(10)
    @Max(20)
    public Integer number = 15;

    @NotEmpty
    @MaxLength(10)
    @MinLength(5)
    public String str = "Hello";

    @NotNull
    Object obj = 10;

    public Main(Integer number, String str) {
        this.number = number;
        this.str = str;
    }

    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException {
        Main main = new Main(20, "Hello");
        Validator<Main> validator = new Validator(main);
        validator.checkMinNumber();


    }
}
