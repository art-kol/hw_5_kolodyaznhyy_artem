package HW5_3;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


public class Validator<T> {

    T checkClass;

    public Validator(T checkClass) {
        this.checkClass = checkClass;
    }

    public void checkMinNumber() throws IllegalAccessException, NoSuchFieldException {
        for(Field field: checkClass.getClass().getDeclaredFields()){

            String typeField = field.getType().getSimpleName();
            System.out.println(typeField);

            if(field.isAnnotationPresent(Min.class) && typeField.equals("Integer")){
                Min annotation = field.getAnnotation(Min.class);
                int min = annotation.value();
                if((int)field.get(checkClass)<min){
                    throw new RuntimeException(field+" не может быть меньше: "+min);
                }
            }

            if(field.isAnnotationPresent(Max.class) && typeField.equals("Integer")){
                Max annotation = field.getAnnotation(Max.class);
                int max = annotation.value();
                if((int)field.get(checkClass)>max){
                    throw new RuntimeException(field+" не может быть больше: "+max);
                }
            }

            if(field.isAnnotationPresent(MinLength.class) && typeField.equals("String")){
                MinLength annotation = field.getAnnotation(MinLength.class);
                int minLenght = annotation.value();
                String strMin = (String)field.get(checkClass);
                if(strMin.length()<minLenght){
                    throw new RuntimeException("Длина "+field+" не может быть меньше: "+minLenght);
                }
            }

            if(field.isAnnotationPresent(MaxLength.class) && typeField.equals("String")){
                MaxLength annotation = field.getAnnotation(MaxLength.class);
                int maxLenght = annotation.value();
                String strMax = (String)field.get(checkClass);
                if(strMax.length()>maxLenght){
                    throw new RuntimeException("Длина "+field+" не может быть больше: "+maxLenght);
                }
            }

            if(field.isAnnotationPresent(NotEmpty.class) && typeField.equals("String")){
                String strMax = (String)field.get(checkClass);
                if(strMax.length()==0){
                    throw new RuntimeException(field+" не может быть пустым");
                }
            }

            if(field.isAnnotationPresent(NotNull.class)){
                Object checkNull = field.get(checkClass);
                System.out.println(checkNull);
                if(checkNull == null){
                    throw new RuntimeException(field+" не может быть null");
                }
            }

        }
    }

}
